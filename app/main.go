package main

import (
	"encoding/json"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/drtz/price-monitor/pkg"
	"gitlab.com/drtz/price-monitor/postgres"
	"gitlab.com/drtz/price-monitor/scraper"

	"context"
	"fmt"
	"net/url"
	"strings"

	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

var pg *postgres.Store
var scr scraper.Scraper

func main() {
	var err error

	scr = scraper.New()

	ctx := appengine.BackgroundContext()

	log.Infof(ctx, "starting server")

	pg, err = postgres.New(os.Getenv("DSN"))
	if err != nil {
		log.Criticalf(ctx, "failed to create postgres store: %v", err.Error())
		os.Exit(1)
	}

	r := mux.NewRouter()
	r.HandleFunc("/trackers/", CreateTracker).Methods(http.MethodPost)
	r.HandleFunc("/trackers/", FindTrackers).Methods(http.MethodGet)
	r.HandleFunc("/run_trackers/", RunTrackers).Methods(http.MethodGet)

	http.Handle("/", r)

	appengine.Main()
}

// CreateTracker creates a Tracker resource
func CreateTracker(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	log.Infof(ctx, "CreateTracker()")

	resp := pkg.NewResponse()

	var t pkg.Tracker

	if err := json.NewDecoder(r.Body).Decode(&t); err != nil {
		log.Errorf(ctx, "decoding request: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	if err := pg.CreateTracker(&t); err != nil {
		log.Errorf(ctx, "creating tracker: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	scrape, err := scr.ScrapeURL(r, t.URL)
	if err != nil {
		log.Errorf(ctx, "scraping: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	t.Price = scrape.Price
	t.StartPrice = scrape.Price
	t.Currency = scrape.Currency
	t.Name = scrape.Name
	t.Brand = scrape.Brand
	t.LastRun = pkg.NullTime(time.Now().UTC())

	if err := pg.UpdateTracker(&t); err != nil {
		log.Errorf(ctx, "updating tracker: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	resp.AddDocuments(t)

	defer resp.Encode(w)
}

// FindTrackers finds trackers
func FindTrackers(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	log.Infof(ctx, "FindTrackers()")

	resp := pkg.NewResponse()

	trackers, err := pg.FindTrackers(postgres.CondAll())
	if err != nil {
		log.Errorf(ctx, "creating tracker: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	for _, t := range trackers {
		log.Infof(ctx, "found tracker: %v", t.ID)
		resp.AddDocuments(t)
	}

	defer resp.Encode(w)
}

func RunTrackers(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	log.Infof(ctx, "RunTrackers()")

	resp := pkg.NewResponse()

	trackers, err := pg.FindTrackers(postgres.CondReadyToRun())
	if err != nil {
		log.Errorf(ctx, "finding trackers: %v", err.Error())
		resp.AddErrors(pkg.NewServerErrorf(err.Error()))
		return
	}

	log.Infof(ctx, "running %v tracker(s)", len(trackers))

	for _, t := range trackers {
		log.Infof(ctx, "running tracker_id=%v", t.ID)

		scrape, err := scr.ScrapeURL(r, t.URL)
		if err != nil {
			log.Errorf(ctx, "scraping: %v", err.Error())
			resp.AddErrors(pkg.NewServerErrorf(err.Error()))
			continue
		}

		// Notify on price drop
		if scrape.Price < t.Price {
			defer func(r *pkg.Response, oldPrice, newPrice uint64) {
				log.Debugf(ctx, "notifying tracker_id=%v", t.ID)
				if err := notify(ctx, t, oldPrice, newPrice); err != nil {
					log.Errorf(ctx, "notifying tracker_id=%v: %v", t.ID, err.Error())
					r.AddErrors(pkg.NewServerErrorf(err.Error()))
				}
			}(&resp, t.Price, scrape.Price)
		}

		t.Price = scrape.Price
		t.LastRun = pkg.NullTime(time.Now().UTC())

		log.Debugf(ctx, "finished running tracker_id=%v", t.ID)
		if err := pg.UpdateTracker(t); err != nil {
			log.Errorf(ctx, "updating tracker: %v", err.Error())
			resp.AddErrors(pkg.NewServerErrorf(err.Error()))
			continue
		}
	}

	defer resp.Encode(w)
}

func notify(ctx context.Context, t *pkg.Tracker, oldPrice, newPrice uint64) error {
	msgText := notifyMessage(*t, oldPrice, newPrice)
	log.Debugf(ctx, "Sending notification: %v", msgText)

	msgData := url.Values{}
	msgData.Set("To", t.PhoneNumber)
	msgData.Set("From", os.Getenv("TWILIO_FROM_NUMBER"))
	msgData.Set("Body", msgText)
	msgDataReader := *strings.NewReader(msgData.Encode())

	client := urlfetch.Client(ctx)
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + os.Getenv("TWILIO_ACCOUNT_SID") + "/Messages.json"
	req, _ := http.NewRequest("POST", urlStr, &msgDataReader)
	req.SetBasicAuth(os.Getenv("TWILIO_ACCOUNT_SID"), os.Getenv("TWILIO_AUTH_TOKEN"))
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	_, err := client.Do(req)
	if err != nil {
		return err
	}

	return nil
}

func notifyMessage(t pkg.Tracker, oldPrice, newPrice uint64) string {
	return fmt.Sprintf("There's a new lower price for %v %v: %v (dropped from %v) -- %v", t.Brand, t.Name, formatPrice(t.Currency, newPrice), formatPrice(t.Currency, oldPrice), t.URL)
}

func formatPrice(currency string, price uint64) string {
	switch currency {
	case "usd":
		return fmt.Sprintf("$%v", float64(price)/100)
	}

	return fmt.Sprintf("%v %v", price, currency)
}
