package pkg

import (
	"fmt"
	"net/http"
)

type Error struct {
	Err    error `json:"message"`
	Status int   `json:"status"`
}

func NewServerErrorf(format string, args ...interface{}) Error {
	return Error{
		fmt.Errorf(format, args...),
		http.StatusInternalServerError,
	}
}

func NewClientErrorf(format string, args ...interface{}) Error {
	return Error{
		fmt.Errorf(format, args...),
		http.StatusBadRequest,
	}
}
