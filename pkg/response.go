package pkg

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	errors []Error
	docs   []interface{}
	status int
}

func NewResponse(docs ...interface{}) Response {
	return Response{
		errors: nil,
		docs:   docs,
		status: 200,
	}
}

func (r *Response) AddErrors(errs ...Error) {
	r.errors = append(r.errors, errs...)
}

func (r *Response) AddDocuments(docs ...interface{}) {
	r.docs = append(r.docs, docs...)
}

func (r *Response) SetStatus(status int) {
	r.status = status
}

func (r *Response) Status() int {
	if len(r.errors) > 0 {
		return r.errors[0].Status
	}
	return r.status
}

func (r Response) MarshalJSON() ([]byte, error) {
	resp := make(map[string]interface{})

	if len(r.errors) > 0 {
		resp["errors"] = r.errors
	}

	for _, doc := range r.docs {
		t, err := getTypeString(doc)
		if err != nil {
			return nil, err
		}

		if _, ok := resp[t]; !ok {
			resp[t] = []interface{}{doc}
		} else {
			resp[t] = append(resp[t].([]interface{}), doc)
		}
	}

	return json.Marshal(resp)
}

func (r Response) Encode(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.Status())
	json.NewEncoder(w).Encode(r)
}

func getTypeString(doc interface{}) (string, error) {
	switch doc.(type) {
	case Tracker:
		return "trackers", nil
	default:
		return "", fmt.Errorf("unknown type")
	}
}
