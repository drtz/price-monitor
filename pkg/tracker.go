package pkg

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type NullTime time.Time

func (n *NullTime) Scan(value interface{}) error {
	if value == nil {
		*n = NullTime(time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC))
		return nil
	}

	if s, ok := value.(time.Time); ok {
		*n = NullTime(s)
		return nil
	}

	return fmt.Errorf("invalid source for NullTime scan: %v", value)
}

func (n NullTime) Value() (driver.Value, error) {
	if n == NullTime(time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC)) {
		return nil, nil
	}

	return time.Time(n), nil
}

// Tracker url struct
type Tracker struct {
	ID          int64    `json:"id"`
	URL         string   `json:"url"`
	PhoneNumber string   `json:"phone_number"`
	Price       uint64   `json:"price"`
	Brand       string   `json:"brand"`
	Name        string   `json:"name"`
	StartPrice  uint64   `json:"start_price"`
	Currency    string   `json:"currency"`
	LastRun     NullTime `json:"last_run"`
}
