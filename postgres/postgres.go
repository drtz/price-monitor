package postgres

import (
	"database/sql"

	"gitlab.com/drtz/price-monitor/pkg"

	sq "github.com/lann/squirrel"
	_ "github.com/lib/pq"
)

type Store struct {
	db *sql.DB
}

func New(dsn string) (*Store, error) {
	var err error
	s := &Store{}

	s.db, err = sql.Open("postgres", dsn)

	return s, err
}

func (s *Store) Ping() error {
	return s.db.Ping()
}

func (s *Store) CreateTracker(t *pkg.Tracker) error {
	query := sq.Insert("tracker").
		Columns("url", "phone_number", "price", "brand", "name", "currency", "last_run", "start_price").
		Values(t.URL, t.PhoneNumber, t.Price, t.Brand, t.Name, t.Currency, t.LastRun, t.StartPrice).
		Suffix("RETURNING \"id\"").
		PlaceholderFormat(sq.Dollar).
		RunWith(s.db)

	err := query.QueryRow().Scan(&t.ID)
	return err
}

func (s *Store) UpdateTracker(t *pkg.Tracker) error {
	query := sq.Update("tracker").
		SetMap(map[string]interface{}{
			"url":          t.URL,
			"brand":        t.Brand,
			"name":         t.Name,
			"phone_number": t.PhoneNumber,
			"price":        t.Price,
			"currency":     t.Currency,
			"last_run":     t.LastRun,
			"start_price":  t.StartPrice,
		}).
		Where(sq.Eq{"id": t.ID}).
		PlaceholderFormat(sq.Dollar).
		RunWith(s.db)

	_, err := query.Query()

	return err
}

func (s *Store) FindTrackers(cond interface{}) ([]*pkg.Tracker, error) {
	query := sq.Select("id", "url", "phone_number", "price", "brand", "name", "currency", "last_run", "start_price").
		From("tracker").
		PlaceholderFormat(sq.Dollar).
		RunWith(s.db)

	if cond != nil {
		query = query.Where(cond)
	}

	rows, err := query.Query()
	if err != nil {
		return nil, err
	}

	var trackers []*pkg.Tracker
	for rows.Next() {
		t := pkg.Tracker{}
		if err := rows.Scan(&t.ID, &t.URL, &t.PhoneNumber, &t.Price, &t.Brand, &t.Name, &t.Currency, &t.LastRun, &t.StartPrice); err != nil {
			return nil, err
		}
		trackers = append(trackers, &t)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return trackers, nil
}

func CondReadyToRun() interface{} {
	return sq.GtOrEq{
		"EXTRACT(EPOCH FROM NOW() - last_run)": 3600, // run at most once an hour
	}
}

func CondAll() interface{} {
	return nil
}
