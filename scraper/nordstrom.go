package scraper

import (
	"net/http"
	"regexp"
	"strings"

	"strconv"

	"fmt"

	"github.com/gocolly/colly"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
)

type nordstromScraper struct{}

func NewNordstromScraper() scraper {
	return &nordstromScraper{}
}

func (s *nordstromScraper) handlesURL(url string) bool {
	return strings.Contains(strings.ToLower(url), "nordstrom")
}

func (s *nordstromScraper) scrapeURL(r *http.Request, url string) (ScrapeResult, error) {
	var foundPrice, foundName, foundBrand bool
	var err error

	ctx := appengine.NewContext(r)

	result := ScrapeResult{}

	collector := colly.NewCollector(colly.UserAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.85 Safari/537.36 "))
	collector.Appengine(r)

	collector.OnHTML(`span[class*="currentPriceString"]`, func(e *colly.HTMLElement) {
		log.Debugf(ctx, "price: "+e.Text)
		result.Currency, result.Price, err = s.decodePrice(e.Text)
		if err == nil {
			foundPrice = true
		}
	})

	collector.OnHTML(`h2[class*=brandTitle]`, func(e *colly.HTMLElement) {
		log.Debugf(ctx, "brand: "+e.Text)
		result.Brand = e.Text
		foundBrand = true
	})

	collector.OnHTML(`h1[class*=productTitle]`, func(e *colly.HTMLElement) {
		log.Debugf(ctx, "name: "+e.Text)
		result.Name = e.Text
		foundName = true
	})

	collector.OnError(func(r *colly.Response, e error) {
		err = e
	})

	collector.Visit(url)
	collector.Wait()

	if !foundPrice {
		if err == nil {
			err = fmt.Errorf("failed to find price")
		}
	}
	if !foundBrand {
		if err == nil {
			err = fmt.Errorf("failed to find brand")
		}
	}
	if !foundName {
		if err == nil {
			err = fmt.Errorf("failed to find name")
		}
	}

	return result, err
}

func (s *nordstromScraper) decodePrice(raw string) (currency string, price uint64, err error) {
	re := regexp.MustCompile("[^0-9]+")
	numStr := re.ReplaceAllStringFunc(raw, func(s string) string {
		switch s {
		case "$":
			currency = "usd"
		}
		return ""
	})

	if currency == "" {
		currency = "usd"
	}

	price, err = strconv.ParseUint(numStr, 10, 64)

	return currency, price, err
}
