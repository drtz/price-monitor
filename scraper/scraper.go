package scraper

import (
	"fmt"
	"net/http"
)

type ScrapeResult struct {
	Brand    string
	Name     string
	Price    uint64
	Currency string
}

type scraper interface {
	handlesURL(string) bool
	scrapeURL(*http.Request, string) (ScrapeResult, error)
}

type Scraper struct {
	scrapers []scraper
}

func (s Scraper) ScrapeURL(r *http.Request, url string) (ScrapeResult, error) {
	for _, scr := range s.scrapers {
		if scr.handlesURL(url) {
			return scr.scrapeURL(r, url)
		}
	}
	return ScrapeResult{}, fmt.Errorf("couldn't find a scraper for url=%v", url)
}

func New() Scraper {
	return Scraper{
		scrapers: []scraper{
			NewNordstromScraper(),
		},
	}
}
